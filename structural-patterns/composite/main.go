package main

func main() {
	folder1 := &Folder{
		name: "Folder1",
	}
	folder1.add(&File{name: "File1.1"})
	folder1.add(&File{name: "File1.2"})

	folder2 := &Folder{
		name: "Folder2",
	}
	folder2.add(&File{name: "File2.1"})
	folder2.add(&File{name: "File2.2"})
	folder2.add(&File{name: "File3.2"})
	folder2.add(folder1)

	folder2.search("my-key")
}
