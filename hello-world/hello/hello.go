package main

import (
	"fmt"
	"golang-playground/hello-world/greetings"
	"log"
	"time"
)

func main() {
	// Set properties of the predefined Logger, including
	// the log entry prefix and a flag to disable printing
	// the time, source file, and line number.
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	// A slice of names.
	names := []string{"Gladys", "Samantha", "Darrin"}

	// Request greeting messages for the names.
	messages, err := greetings.Hellos(names)
	if err != nil {
		log.Fatal(err)
	}

	// If no error was returned, print the returned message
	// to the console.
	fmt.Println(messages)

	test := time.Time{}
	fmt.Println(test.Format("1994-11-05T13:15:30Z"))

	test2 := time.Now()
	fmt.Println(test2.UTC().String())

}
