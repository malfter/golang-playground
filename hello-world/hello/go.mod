module golang-playground/hello

go 1.22

require golang-playground/hello-world/greetings v0.0.0

replace golang-playground/hello-world/greetings => ./../greetings
