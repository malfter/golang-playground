package main

type CompanyA struct {
}

func (a *CompanyA) makeChocolate() IChocolate {
	return &CompanyAChocolate{
		Chocolate: Chocolate{
			logo:       "company a",
			sugarLevel: 10,
		},
	}
}

func (a *CompanyA) makeWineGum() IWineGum {
	return &CompanyAWineGum{
		WineGum: WineGum{
			logo:       "company a",
			sugarLevel: 5,
		},
	}
}
