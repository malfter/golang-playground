package main

type IWineGum interface {
	setLogo(logo string)
	setSugarLevel(sugarLevel int)
	getLogo() string
	getSugarLevel() int
}

type WineGum struct {
	logo       string
	sugarLevel int
}

func (wg *WineGum) setLogo(logo string) {
	wg.logo = logo
}

func (wg *WineGum) getLogo() string {
	return wg.logo
}

func (wg *WineGum) setSugarLevel(sugarLevel int) {
	wg.sugarLevel = sugarLevel
}

func (wg *WineGum) getSugarLevel() int {
	return wg.sugarLevel
}
