package main

import "fmt"

type ICandyFactory interface {
	makeChocolate() IChocolate
	makeWineGum() IWineGum
}

func GetCandyFactory(brand string) (ICandyFactory, error) {
	if brand == "company a" {
		return &CompanyA{}, nil
	}

	if brand == "company b" {
		return &CompanyB{}, nil
	}

	return nil, fmt.Errorf("wrong brand type passed")
}
