package main

type CompanyB struct {
}

func (a *CompanyB) makeChocolate() IChocolate {
	return &CompanyBChocolate{
		Chocolate: Chocolate{
			logo:       "company b",
			sugarLevel: 20,
		},
	}
}

func (a *CompanyB) makeWineGum() IWineGum {
	return &CompanyBWineGum{
		WineGum: WineGum{
			logo:       "company b",
			sugarLevel: 10,
		},
	}
}
