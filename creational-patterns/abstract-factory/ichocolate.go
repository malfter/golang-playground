package main

type IChocolate interface {
	setLogo(logo string)
	setSugarLevel(sugarLevel int)
	getLogo() string
	getSugarLevel() int
}

type Chocolate struct {
	logo       string
	sugarLevel int
}

func (c *Chocolate) setLogo(logo string) {
	c.logo = logo
}

func (c *Chocolate) getLogo() string {
	return c.logo
}

func (c *Chocolate) setSugarLevel(sugarLevel int) {
	c.sugarLevel = sugarLevel
}

func (c *Chocolate) getSugarLevel() int {
	return c.sugarLevel
}
