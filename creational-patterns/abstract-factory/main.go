package main

import "fmt"

func main() {
	companyAFactory, _ := GetCandyFactory("company a")
	companyBFactory, _ := GetCandyFactory("company b")

	companyAChocolate := companyAFactory.makeChocolate()
	companyAWineGum := companyBFactory.makeWineGum()

	companyBChocolate := companyBFactory.makeChocolate()
	companyBWineGum := companyBFactory.makeWineGum()

	printChocolateDetails(companyAChocolate)
	printWineGumDetails(companyAWineGum)

	printChocolateDetails(companyBChocolate)
	printWineGumDetails(companyBWineGum)
}

func printChocolateDetails(s IChocolate) {
	fmt.Printf("Chocolate Logo: %s", s.getLogo())
	fmt.Println()
	fmt.Printf("Chocolate sugar level: %d", s.getSugarLevel())
	fmt.Println()
}

func printWineGumDetails(s IWineGum) {
	fmt.Printf("Wine gum Logo: %s", s.getLogo())
	fmt.Println()
	fmt.Printf("Wine gum sugar level: %d", s.getSugarLevel())
	fmt.Println()
}
