package main

import (
	"fmt"
	"sync"
)

var lock = &sync.Mutex{}

type single struct {
}

var singleInstance *single

func getInstance(call string) *single {
	if singleInstance == nil {
		lock.Lock()
		defer lock.Unlock()
		if singleInstance == nil {
			fmt.Println(fmt.Sprintf("[%s] Creating single instance now.", call))
			singleInstance = &single{}
		} else {
			fmt.Println(fmt.Sprintf("[%s] Single instance already created.", call))
		}
	} else {
		fmt.Println(fmt.Sprintf("[%s] Single instance already created.", call))
	}
	return singleInstance
}
