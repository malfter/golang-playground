package main

import (
	"fmt"
	"strconv"
)

func main() {

	for i := 0; i < 30; i++ {
		go getInstance(strconv.Itoa(i))
	}

	// Scanln is similar to Scan, but stops scanning at a newline and
	// after the final item there must be a newline or EOF.
	_, _ = fmt.Scanln()
}
