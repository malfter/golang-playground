package main

import "fmt"

func getVehicle(vehicleType string) (IVehicle, error) {
	if vehicleType == "car" {
		return newCar(), nil
	}
	if vehicleType == "bicycle" {
		return newBicycle(), nil
	}
	return nil, fmt.Errorf("wrong vehicle type passed")
}
