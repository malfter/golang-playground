package main

import "fmt"

func main() {
	car, _ := getVehicle("car")
	bicycle, _ := getVehicle("bicycle")

	printDetails(car)
	printDetails(bicycle)
}

func printDetails(v IVehicle) {
	fmt.Printf("Vehicle: %s", v.getName())
	fmt.Println()
	fmt.Printf("Wheels: %d", v.getWheels())
	fmt.Println()
}
