package main

type bicycle struct {
	Vehicle
}

func newBicycle() IVehicle {
	return &bicycle{
		Vehicle: Vehicle{
			name:   "bicycle",
			wheels: 2,
		},
	}
}
