package main

type IVehicle interface {
	setName(name string)
	setWheels(wheels int)
	getName() string
	getWheels() int
}
