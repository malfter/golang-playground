package main

type Vehicle struct {
	name   string
	wheels int
}

func (v *Vehicle) setName(name string) {
	v.name = name
}

func (v *Vehicle) getName() string {
	return v.name
}

func (v *Vehicle) setWheels(wheels int) {
	v.wheels = wheels
}

func (v *Vehicle) getWheels() int {
	return v.wheels
}
