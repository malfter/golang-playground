package main

type Car struct {
	Vehicle
}

func newCar() IVehicle {
	return &Car{
		Vehicle: Vehicle{
			name:   "car",
			wheels: 4,
		},
	}
}
