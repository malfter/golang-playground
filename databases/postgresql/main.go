package main

import (
	"database/sql"
	"flag"
	"golang-playground/databases/postgreql/handler"
	"golang-playground/databases/postgreql/migrations"
	"golang-playground/databases/postgreql/service"
	"golang-playground/databases/postgreql/store"
	"log"
	"net/http"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file" // Import the file source driver
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"
	_ "github.com/lib/pq" // PostgreSQL driver
)

var (
	dbMigrateSource string
	dbMigrate       *migrate.Migrate
)

func main() {

	flag.StringVar(&dbMigrateSource, "migrate-source", "go-bindata", "Migrate Source, e.g. 'go-bindata' or 'file'")
	flag.Parse()

	// Database connection
	dataSourceName := "postgres://taskuser:password@localhost:5432/taskmanager?sslmode=disable"

	db, err := sql.Open("postgres", dataSourceName)
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	// Run migrations
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		log.Fatalf("Could not start SQL driver: %v", err)
	}

	switch {
	case dbMigrateSource == "file":
		dbMigrate, err = migrate.NewWithDatabaseInstance(
			"file://migrations",
			"postgres",
			driver,
		)
		if err != nil {
			log.Fatalf("Could not start migration: %v", err)
		}
	case dbMigrateSource == "go-bindata":

		sourceInstance, err := bindata.WithInstance(
			bindata.Resource(
				migrations.AssetNames(),
				migrations.Asset,
			),
		)
		if err != nil {
			log.Fatalf("Could not create sourceInstance: %v", err)
		}

		dbMigrate, err = migrate.NewWithInstance(
			"go-bindata",
			sourceInstance,
			"postgres",
			driver,
		)
		if err != nil {
			log.Fatalf("Could not start migration: %v", err)
		}
	default:
		log.Fatal("No vaild migrate source")
	}

	log.Printf("Migrate source: %s", dbMigrateSource)
	err = dbMigrate.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Fatalf("Migration failed: %v", err)
	}

	// Initialize the store
	taskStore := &store.SQLTaskStore{DB: db}
	taskService := service.TaskService{TaskStore: taskStore}
	taskHandler := handler.TaskHandler{TaskService: taskService}

	http.HandleFunc("/tasks", taskHandler.CreateTaskHandler)
	http.HandleFunc("/tasks/list", taskHandler.ListTasksHandler)

	log.Println("Server listen on :8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("Server failed to start: %v", err)
	}
}
