#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE DATABASE taskmanager;
  CREATE USER taskuser WITH ENCRYPTED PASSWORD 'password';
  GRANT ALL PRIVILEGES ON DATABASE taskmanager TO taskuser;
  ALTER DATABASE taskmanager OWNER TO taskuser;
EOSQL
