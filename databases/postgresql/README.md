# postgresql

In this directory you will find a small Go application that demonstrates the use of a database in Go with automatic database schema migration.

- [https://github.com/golang-migrate/migrate]
- [https://github.com/go-bindata/go-bindata]

# How to use

```bash
# Start postgres db (localhost:5432)
# User: taskuser Password: password
docker compose up -d

# Start http server on port :8080
go run main.go

# Use different migration source
# default: `go-bindate`
# see also ./migrations/README.md
go run main.go -migrate-source file

# Cleanup
docker compose down
```

Task Manager Request Examples:

- [./tests.http](./tests.http)
