// Code generated for package migrations by go-bindata DO NOT EDIT. (@generated)
// sources:
// 000001_create_tasks_table.down.sql
// 000001_create_tasks_table.up.sql
package migrations

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

// Name return file name
func (fi bindataFileInfo) Name() string {
	return fi.name
}

// Size return file size
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}

// Mode return file mode
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}

// Mode return file modify time
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}

// IsDir return file whether a directory
func (fi bindataFileInfo) IsDir() bool {
	return fi.mode&os.ModeDir != 0
}

// Sys return file is sys mode
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var __000001_create_tasks_tableDownSql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x72\x09\xf2\x0f\x50\x08\x71\x74\xf2\x71\x55\xf0\x74\x53\x70\x8d\xf0\x0c\x0e\x09\x56\x28\x49\x2c\xce\x2e\xb6\xe6\x02\x04\x00\x00\xff\xff\x3f\xd0\xe5\x8d\x1c\x00\x00\x00")

func _000001_create_tasks_tableDownSqlBytes() ([]byte, error) {
	return bindataRead(
		__000001_create_tasks_tableDownSql,
		"000001_create_tasks_table.down.sql",
	)
}

func _000001_create_tasks_tableDownSql() (*asset, error) {
	bytes, err := _000001_create_tasks_tableDownSqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "000001_create_tasks_table.down.sql", size: 28, mode: os.FileMode(436), modTime: time.Unix(1730882058, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var __000001_create_tasks_tableUpSql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x4c\xcb\x31\xcb\x83\x30\x10\x06\xe0\x3d\xbf\xe2\x1d\x15\x9c\x3e\x70\xfa\xa6\xab\x3d\x69\x68\xb4\x72\x39\x8b\x4e\x22\x9a\x21\xb4\xb4\x45\xf3\xff\x29\x74\x28\x9d\x1f\x9e\x4a\x98\x94\xa1\x74\x70\x0c\x5b\xa3\xbd\x28\x78\xb0\x5e\x3d\xd2\xbc\xdf\x76\x64\x06\x71\x85\x67\xb1\xe4\xd0\x89\x6d\x48\x46\x9c\x79\x2c\x0c\x52\x4c\xf7\x80\x2b\x49\x75\x22\xc9\xfe\xca\x32\xff\xf4\xb6\x77\xae\x30\x58\xc3\xbe\x6c\xf1\x95\xe2\xf3\x01\xe5\x41\x7f\x6d\xd9\xc2\x9c\xc2\x3a\xcd\x09\x6a\x1b\xf6\x4a\x4d\x87\x23\xd7\xd4\x3b\x45\xd5\x8b\x70\xab\xd3\x57\x4c\xfe\x6f\xde\x01\x00\x00\xff\xff\x9f\x42\xc0\x04\xa8\x00\x00\x00")

func _000001_create_tasks_tableUpSqlBytes() ([]byte, error) {
	return bindataRead(
		__000001_create_tasks_tableUpSql,
		"000001_create_tasks_table.up.sql",
	)
}

func _000001_create_tasks_tableUpSql() (*asset, error) {
	bytes, err := _000001_create_tasks_tableUpSqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "000001_create_tasks_table.up.sql", size: 168, mode: os.FileMode(436), modTime: time.Unix(1730882058, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"000001_create_tasks_table.down.sql": _000001_create_tasks_tableDownSql,
	"000001_create_tasks_table.up.sql":   _000001_create_tasks_tableUpSql,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//
//	data/
//	  foo.txt
//	  img/
//	    a.png
//	    b.png
//
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}

var _bintree = &bintree{nil, map[string]*bintree{
	"000001_create_tasks_table.down.sql": &bintree{_000001_create_tasks_tableDownSql, map[string]*bintree{}},
	"000001_create_tasks_table.up.sql":   &bintree{_000001_create_tasks_tableUpSql, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}
