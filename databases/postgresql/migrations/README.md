# Create or Update `bindata.go`

```bash
# Install go-bindata
go install -a -v github.com/go-bindata/go-bindata/...@latest

# Generate bindata.go
go-bindata -pkg migrations -ignore README.md -ignore bindata.go .
```
