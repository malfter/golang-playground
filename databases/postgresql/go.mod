module golang-playground/databases/postgreql

go 1.22.0

toolchain go1.23.4

require (
	github.com/golang-migrate/migrate/v4 v4.18.1
	github.com/lib/pq v1.10.9
)

require (
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)
