module golang-playground/mime-types

go 1.22

require github.com/gabriel-vasile/mimetype v1.4.7

require (
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.27.0 // indirect
)

require (
	github.com/jedib0t/go-pretty/v6 v6.6.6
	golang.org/x/net v0.31.0 // indirect
)
