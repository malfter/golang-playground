package main

import (
	"bufio"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/gabriel-vasile/mimetype"
	"github.com/jedib0t/go-pretty/v6/table"
)

const testFilesDir = "files"

func main() {
	entries, err := os.ReadDir(testFilesDir)
	if err != nil {
		fmt.Printf("Error when reading the directory: %v\n", err)
		return
	}

	fileInfos := make([]FileInfo, 0, len(entries))
	for _, entry := range entries {
		if entry.IsDir() {
			continue
		}

		info, err := entry.Info()
		if err != nil {
			fmt.Printf("Error when retrieving the file information for %s: %v\n", entry.Name(), err)
			continue
		}

		filePath := filepath.Join(testFilesDir, entry.Name())
		content, err := readFileContent(filePath)
		if err != nil {
			fmt.Printf("Error reading the file %s: %v\n", filePath, err)
			continue
		}

		mime := mimetype.Detect(content)

		fileInfos = append(fileInfos, FileInfo{Name: entry.Name(), Info: info, Mime: mime})
	}
	printTable(fileInfos)
}

func readFileContent(filePath string) ([]byte, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanBytes)
	var content []byte
	for scanner.Scan() {
		content = append(content, scanner.Bytes()...)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return content, nil
}

type FileInfo struct {
	Name string
	Info fs.FileInfo
	Mime *mimetype.MIME
}

func printTable(files []FileInfo) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"File", "MIME Type"})
	for _, file := range files {
		t.AppendRow(table.Row{file.Name, file.Mime.String()})
	}
	t.Render()
}
